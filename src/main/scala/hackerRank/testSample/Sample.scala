package hackerRank.testSample

object Sample extends App {
  def oddNumbers(l: Int, r: Int): Array[Int] = {
    (l to r).filter(_ % 2 == 1).toArray

  }
}
