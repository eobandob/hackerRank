package hackerRank.dataStructure.arrays

/**
  * Created by londo on 16/09/16.
  * https://www.hackerrank.com/challenges/sparse-arrays
  */
object SparceArray {

  def main(args: Array[String]) {
    val sc = new java.util.Scanner (System.in);
    val n = sc.nextInt();
    val words = for(s <- 0 until n) yield sc.next()
    val wordsMap = words.groupBy(s => s)
    val q = sc.nextInt()
    for(s <- 0 until q) {
      println(wordsMap.get(sc.next()).getOrElse(Nil).size)
    }
  }
}
