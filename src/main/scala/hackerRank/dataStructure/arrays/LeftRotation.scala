package hackerRank.dataStructure.arrays

/**
  * Created by londo on 16/09/16.
  * https://www.hackerrank.com/challenges/array-left-rotation
  */
object LeftRotation {

  def main(args: Array[String]) {
    val input = scala.io.Source.fromInputStream(System.in).mkString
    val (rotate, list) = read(input)
    val result = rotation(rotate, list)
    println(result.mkString(" "))
  }

  def read(input: String): (Int, List[Int]) = {
    val sc = new java.util.Scanner(input)
    val n = sc.nextInt()
    val arr = new Array[Int](n)
    val rotate = sc.nextInt()
    for(arr_i <- 0 until n) {
      arr(arr_i) = sc.nextInt()
    }
    rotate -> arr.toList
  }

  def rotation(rotation: Int, list: List[Int]): List[Int] = {
    list.drop(rotation) ++ list.take(rotation)
  }

}