package hackerRank.dataStructure.arrays

/**
  * Created by londo on 16/09/16.
  * https://www.hackerrank.com/challenges/arrays-ds
  */
object Arrays {

  def main(args: Array[String]) {
    val sc = new java.util.Scanner (System.in);
    val n = sc.nextInt();
    val arr = new Array[Int](n);
    for(arr_i <- 0 to n-1) {
      arr(arr_i) = sc.nextInt();
    }
    println(arr.reverse.mkString(" "))
  }
}
