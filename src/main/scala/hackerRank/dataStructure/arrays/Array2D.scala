package hackerRank.dataStructure.arrays

/**
  * Created by londo on 16/09/16.
  * https://www.hackerrank.com/challenges/2d-array
  */
object Array2D {

  def main(args: Array[String]) {
    val input = scala.io.Source.fromInputStream(System.in).mkString
    val array = read(input)
    val res = process(array)
    println(res)
  }

  def read(input: String): List[List[Int]] = {
    val sc = new java.util.Scanner(input)
    val arr = Array.ofDim[Int](6,6)
    val arreglo = for(arr_i <- 0 to 6-1) yield {
      for(arr_j <- 0 to 6-1) {
        arr(arr_i)(arr_j) = sc.nextInt()
      }
      arr(arr_i).toList
    }
    arreglo.toList
  }

  def process(array: List[List[Int]]): Int = {
    val values = for {
      x <- 1 to 4
      y <- 1 to 4
    } yield hourglass(x, y, array)
    values.max
  }

  def hourglass(x: Int, y: Int, arr: List[List[Int]]): Int = {
    arr(x-1)(y-1) + arr(x-1)(y) + arr(x-1)(y+1) + arr(x)(y) + arr(x+1)(y-1) +
      arr(x+1)(y) + arr(x+1)(y+1)
  }
}