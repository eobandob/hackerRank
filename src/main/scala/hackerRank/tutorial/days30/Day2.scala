package hackerRank.tutorial.days30

/**
  * Created by londo on 22/09/16.
  */
object Day2 {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner (System.in)
    val mealCost = sc.nextDouble()
    val tip = sc.nextInt()
    val tax = sc.nextInt()

    val total = mealCost + (mealCost * tip / 100) + (mealCost * tax / 100)
    println(s"The total meal cost is ${math.round(total)} dollars.")
  }
}
