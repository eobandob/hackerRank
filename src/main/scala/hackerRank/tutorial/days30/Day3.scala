package hackerRank.tutorial.days30

/**
  * Created by londo on 26/09/16.
  * https://www.hackerrank.com/challenges/30-conditional-statements
  */
object Day3 {

  def main(args: Array[String]): Unit = {
    val input = scala.io.Source.fromInputStream(System.in).mkString
    val number = read(input)
    val res = isWeird(number)
    println(res)
  }

  def read(input: String): Int = {
    val sc = new java.util.Scanner(input)
    sc.nextInt()
  }

  def isWeird(number: Int): String = {
    number match {
      case par if par % 2 == 1 => "Weird"
      case impar if impar % 2 == 0 && impar >= 6 && impar <= 20 => "Weird"
      case _ => "Not Weird"
    }
  }

}
