package hackerRank.tutorial.days30

/**
  * Created by londo on 22/09/16.
  */
object Day0 {
  def main(args: Array[String]): Unit = {
    val sc = scala.io.Source.fromInputStream(System.in)
    println("Hello, World.")
    println(sc.mkString)
  }

}
