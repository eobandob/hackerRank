package hackerRank.tutorial.days30;

import java.util.Scanner;

/**
 * Created by londo on 22/09/16.
 */
public class Day1 {

    public static void main(String[] args) {
        int i = 4;
        double d = 4.0;
        String s = "HackerRank ";

        Scanner scan = new Scanner(System.in);
        /* Declare second integer, double, and String variables. */
        int entero = scan.nextInt();
        double doble = scan.nextDouble();
        String texto = "";
        while (scan.hasNext()) {
            texto += scan.next() + " ";
        }

        /* Print the sum of both integer variables on a new line. */
        System.out.println(i + entero);

        /* Print the sum of the double variables on a new line. */
        System.out.println(d + doble);

        /* Concatenate and print the String variables on a new line;
        	the 's' variable above should be printed first. */
        System.out.println(s + texto);
//        scan.close();
    }
}