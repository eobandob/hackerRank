package hackerRank.tutorial.days30

/**
  * Created by londo on 26/09/16.
  * https://www.hackerrank.com/challenges/30-class-vs-instance/copy-from/28832994
  */
object Day4 {

  def main(args: Array[String]) {
    val T = scala.io.StdIn.readInt()
    val i = 0
    for(i <- 1 to T){
      val age = scala.io.StdIn.readInt()
      val p = new Person(age)
      p.amIOld()
      for(j <- 1 to 3){
        p.yearPasses()
      }
      p.amIOld()
      System.out.println()
    }

  }
}

class Person {

  var age: Int = 0

  def this(initialAge:Int) = {
    this()
    if (initialAge < 0) println("Age is not valid, setting age to 0.")
    else age = initialAge
  }

  def amIOld(): Unit = {
    if (age < 13) println("You are young.")
    else {
      if (age < 18) println("You are a teenager.")
      else println("You are old.")
    }
  }

  def yearPasses(): Unit = {
    age += 1
  }

}
