package hackerRank.algorithms.implementation

/**
 * Created by londo on 17/09/16.
 * https://www.hackerrank.com/challenges/breaking-best-and-worst-records/problem
 */
object BreakingRecords {

  // Complete the breakingRecords function below.
  def breakingRecords(scores: Array[Int]): Array[Int] = {
    @scala.annotation.tailrec
    def recordsRec(scores: scala.collection.immutable.List[Int], maxNum: Int, minNum: Int, max: Int, min: Int): Array[Int] = {
      if (scores.isEmpty) Array(max, min)
      else {
        val score = scores.head
        val (new_max, new_maxNum) = if (score > maxNum) (max + 1, score) else (max, maxNum)
        val (new_min, new_minNum) = if (score < minNum) (min + 1, score) else (min, minNum)
        println(s"#########  score: $score, maxNum: $new_maxNum, minNum: $new_minNum, max: $new_max, min: $new_min")
        recordsRec(scores.tail, new_maxNum, new_minNum, new_max, new_min)
      }
    }
    val first = scores.head
    recordsRec(scores.tail.toList, first, first, 0, 0)

  }

  def main(args: Array[String]): Unit = {
//    val stdin = scala.io.StdIn
//
//    val printWriter = new PrintWriter(sys.env("OUTPUT_PATH"))
//
//    val n = stdin.readLine.trim.toInt
//
//    val scores = stdin.readLine.split(" ").map(_.trim.toInt)
    val scores = Array(10, 5, 20, 20, 4, 5, 2, 25, 1)
    println(s"#### ${scores.toList}")
    val result = breakingRecords(scores)

    println(result.mkString(" "))
//    printWriter.println(result.mkString(" "))
//
//    printWriter.close()
  }
}
