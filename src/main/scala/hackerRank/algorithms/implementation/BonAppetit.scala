package hackerRank.algorithms.implementation

import scala.io.Source

/**
  * Created by londo on 17/09/16.
  * https://www.hackerrank.com/challenges/bon-appetit
  */
object BonAppetit {

  def main(args: Array[String]) {
    val (notEaten, items, charged) = read(Source.fromInputStream(System.in).mkString)
    val result = process(items, charged, notEaten)
    println(result)
  }

  def read(input: String): (Int, List[Int], Int) = {
    val sc = new java.util.Scanner(input)
    val n = sc.nextInt()
    val k = sc.nextInt()
    val arr = new Array[Int](n)
    for(arr_i <- 0 until n) {
      arr(arr_i) = sc.nextInt()
    }
    val b = sc.nextInt()
    (k, arr.toList, b)
  }

  def process(items: List[Int], charged: Int, notEaten: Int): String = {
    val total = (items.sum - items(notEaten)) / 2
    if (charged == total)
      "Bon Appetit"
    else
      s"${charged - total}"
  }
}
