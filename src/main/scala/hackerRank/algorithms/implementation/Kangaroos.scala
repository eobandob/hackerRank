package hackerRank.algorithms.implementation

object Kangaroos {

  // Complete the kangaroo function below.
  def kangaroo(x1: Int, v1: Int, x2: Int, v2: Int): String = {
    if (x1 == x2) "YES"
    else if (x1 < x2 && v1 > v2) jumps(x1, v1, x2, v2)
    else if (x2 < x1 && v2 > v1) jumps(x2, v2, x1, v1)
    else "NO"
  }

  def jumps(x1: Int, v1: Int, x2: Int, v2: Int): String = {
    var i = 0
    while ((x1 + (i * v1)) < (x2 + (i * v2))) {
      i += 1
    }
    if ((x1 + (i * v1)) == (x2 + (i * v2))) "YES"
    else "NO"
  }

  def main(args: Array[String]): Unit = {
//    val stdin = scala.io.StdIn
//
//    val printWriter = new PrintWriter(sys.env("OUTPUT_PATH"))
//
//    val x1V1X2V2 = stdin.readLine.split(" ")
//
//    val x1 = x1V1X2V2(0).trim.toInt
//
//    val v1 = x1V1X2V2(1).trim.toInt
//
//    val x2 = x1V1X2V2(2).trim.toInt
//
//    val v2 = x1V1X2V2(3).trim.toInt
//
//    val result = kangaroo(x1, v1, x2, v2)
//
//    printWriter.println(result)
//
//    printWriter.close()
    val (x1, v1, x2, v2) = (0, 3, 4, 2)
    val result = kangaroo(x1, v1, x2, v2)

    val (x1a, v1a, x2a, v2a) = (0, 2, 5, 3)
    val resulta = kangaroo(x1a, v1a, x2a, v2a)

    println(result)
    println(resulta)
  }
}
