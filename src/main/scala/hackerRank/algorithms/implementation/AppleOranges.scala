package hackerRank.algorithms.implementation

import scala.io.Source

/**
  * Created by londo on 17/09/16.
  * https://www.hackerrank.com/challenges/apple-and-orange/problem
  */

object AppleOranges {

  // Complete the countApplesAndOranges function below.
  def countApplesAndOranges(s: Int, t: Int, a: Int, b: Int, apples: Array[Int], oranges: Array[Int]): Unit = {
    val apInHouse = apples.map(_ + a).filter(num => num >= s && num <= t)
    val orInHouse = oranges.map(_ + b).filter(num => num >= s && num <= t)
    println(apInHouse.length)
    println(orInHouse.length)
  }

  def main(args: Array[String]): Unit = {
//    val stdin = scala.io.StdIn
//
//    val st = stdin.readLine.split(" ")
//
//    val s = st(0).trim.toInt
//
//    val t = st(1).trim.toInt
//
//    val ab = stdin.readLine.split(" ")
//
//    val a = ab(0).trim.toInt
//
//    val b = ab(1).trim.toInt
//
//    val mn = stdin.readLine.split(" ")
//
//    val m = mn(0).trim.toInt
//
//    val n = mn(1).trim.toInt
//
//    val apples = stdin.readLine.split(" ").map(_.trim.toInt)
//
//    val oranges = stdin.readLine.split(" ").map(_.trim.toInt)

    val s = 7
    val t = 10
    val a = 4
    val b = 12
    val m = 3
    val n = 2
    val apples = Array(2,3,-4)
    val oranges = Array(3,-2,-4)
    countApplesAndOranges(s, t, a, b, apples, oranges)
  }
}
