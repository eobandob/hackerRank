package hackerRank.algorithms.strings

/**
  * Created by londo on 27/09/16.
  */
object BeautifulBinaryString {

  def main(args: Array[String]): Unit = {
    val input = scala.io.Source.fromInputStream(System.in).mkString
    val binaryString = read(input)
    val res = lessMovements(binaryString)
    println(res)
  }

  def read(input: String): String = {
    val sc = new java.util.Scanner(input)
    sc.nextInt()
    sc.next()
  }

  def lessMovements(binary: String): Int = {
    def stream(init: List[Char], remaining: List[Char], counter:Int): List[Int] = {
      remaining match {
        case Nil => List(counter)
        case a :: Nil => stream(init ::: List(a), Nil, counter)
        case a :: b :: Nil => stream(init ::: List(a, b), Nil, counter)
        case '0' :: '1' :: '0' :: '1' :: '0' :: tail =>
          stream(init ::: List('0', '1', '1', '1'), '0' :: tail, counter + 1)
        case '0' :: '1' :: '0' :: tail =>
          stream(init.dropRight(2), init.takeRight(2) ::: List('1', '1', '0') ::: tail, counter + 1) ++
            stream(init.dropRight(2), init.takeRight(2) ::: List('0', '0', '0') ::: tail, counter + 1) ++
            stream(init.dropRight(2), init.takeRight(2) ::: List('0', '1', '1') ::: tail, counter + 1)
        case a :: '0' :: '1' :: tail => stream(init ::: List(a), List('0', '1') ::: tail, counter)
        case a :: b :: '0' :: tail => stream(init ::: List(a, b), '0' :: tail, counter)
        case a :: b :: c :: tail => stream(init ::: List(a, b, c), tail, counter)
      }
    }
    stream(Nil, binary.toList, 0).sortBy(i => i).headOption.getOrElse(0)
  }

}
