package hackerRank.algorithms.strings

/**
  * Created by londo on 26/09/16.
  * https://www.hackerrank.com/challenges/reduced-string
  */
object SuperReduced {
  def main(a: Array[String]): Unit = {
    val input = scala.io.Source.fromInputStream(System.in).mkString
    val res = reduce(input)
    println(res)
  }

  def reduce(string: String): String = {
    @scala.annotation.tailrec
    def rec(init: List[Char], remaining: List[Char]): String = {
      remaining match {
        case Nil => init.mkString
        case a :: Nil => rec(init ::: List(a), Nil)
        case a :: b :: tail if a == b => rec(Nil, init ::: tail)
        case a :: b :: tail => rec( init ::: List(a), b :: tail)
      }
    }

    val res = rec(Nil, string.toList)
    if (res.isEmpty) "Empty String" else res
  }
}
