package hackerRank.algorithms.strings

/**
  * Created by londo on 27/09/16.
  */
object Gemstones {

  def main(args: Array[String]): Unit = {
    val input = scala.io.Source.fromInputStream(System.in).mkString
    val rocks = read(input)
    val res = commonElements(rocks)
    println(res.length)
  }

  def read(input: String): List[String] = {
    val sc = new java.util.Scanner(input)
    val n = sc.nextInt()
    val list = for (i <- 1 to n) yield sc.next()
    list.toList
  }

  def commonElements(rocks: List[String]): String = {
    if (rocks.isEmpty) ""
    else {
      rocks.foldRight(rocks.head)((s, r) => r.intersect(s))
    }
  }

}
