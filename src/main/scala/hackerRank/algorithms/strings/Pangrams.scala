package hackerRank.algorithms.strings

/**
  * Created by londo on 26/09/16.
  * https://www.hackerrank.com/challenges/pangrams
  */
object Pangrams {
  def main(a: Array[String]): Unit = {
    val input = scala.io.Source.fromInputStream(System.in).mkString
    val res = isPangram(input)
    println(if (res) "pangram" else "not pangram")
  }

  def isPangram(text: String): Boolean = {
    val texto = text.toLowerCase.replaceAll(" ", "")
    texto.matches("[a-z]+") && texto.groupBy(c => c).size == 26
  }

}
