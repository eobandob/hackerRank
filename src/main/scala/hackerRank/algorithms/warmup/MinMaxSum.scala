package hackerRank.algorithms.warmup

/**
 * Created by londo on 2021-03-10.
 * https://www.hackerrank.com/challenges/mini-max-sum/problem
 */
object MinMaxSum {

  // Complete the miniMaxSum function below.
  def miniMaxSum(arr: Array[Int]): Unit = {
    val sortedArr = arr.sorted.map(BigInt(_))
    val min = sortedArr.take(4).sum
    val max = sortedArr.takeRight(4).sum
    println(s"$min $max")
  }

  def main(args: Array[String]): Unit = {
//    val stdin = scala.io.StdIn

//    val arr = stdin.readLine.split(" ").map(_.trim.toInt)
    val arr = args.map(_.toInt)
    miniMaxSum(arr)
  }
}
