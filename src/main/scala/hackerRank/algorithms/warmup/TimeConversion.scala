package hackerRank.algorithms.warmup

/**
  * Created by londo on 17/09/16.
  * https://www.hackerrank.com/challenges/time-conversion
  */
object TimeConversion {

  def main(args: Array[String]) {
    val sc = new java.util.Scanner (System.in)
    println(timeConvertion(sc.next()))
  }

  def timeConvertion(hora: String): String = {
    hora match {
      case "12:00:00PM" => "12:00:00"
      case s if s.startsWith("12") && s.endsWith("AM") => "00" + s.drop(2).dropRight(2).mkString("")
      case s if s.endsWith("PM") && !s.startsWith("12") =>
        f"${s.take(2).mkString("").toInt + 12}%2.0f" + s.drop(2).dropRight(2).mkString("")
      case s => s.dropRight(2).mkString("")
    }
  }
}
