package hackerRank.algorithms.warmup

/**
  * Created by londo on 17/09/16.
  * https://www.hackerrank.com/challenges/plus-minus
  */
object PlusMinus {

  def main(args: Array[String]) {
    val input = scala.io.Source.fromInputStream(System.in).mkString
    val (size, list) = read(input)
    val (pos, neg, zero) = process(size, list)
    printFraction(pos)
    printFraction(neg)
    printFraction(zero)
  }

  def read(input: String): (Int, List[Int]) = {
    val sc = new java.util.Scanner(input)
    val n = sc.nextInt()
    val arr = new Array[Int](n)
    for(arr_i <- 0 until n) {
      arr(arr_i) = sc.nextInt()
    }
    n -> arr.toList
  }

  def process(size: Int, list: List[Int]): (Double, Double, Double) = {
    def fraction = (l: List[Int]) => 1.0 * l.size / size
    val group = list.groupBy(i => math.signum(i))
    ( fraction(group.getOrElse(1, Nil)),
      fraction(group.getOrElse(-1, Nil)),
      fraction(group.getOrElse(0, Nil))
    )
  }

  def printFraction(num: Double):Unit = {
    println(f"$num%1.6f")
  }
}
