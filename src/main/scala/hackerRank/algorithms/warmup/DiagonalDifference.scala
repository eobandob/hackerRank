package hackerRank.algorithms.warmup

/**
  * Created by londo on 17/09/16.
  * https://www.hackerrank.com/challenges/diagonal-difference
  */
object DiagonalDifference {

  type Matrix = List[List[Int]]

  def main(args: Array[String]) {
    val input = scala.io.Source.fromInputStream(System.in).mkString
    val (size, matrix) = read(input)
    val difference = process(size, matrix)
    println(difference)
  }

  def read(input: String): (Int, Matrix) = {
    val sc = new java.util.Scanner(input)
    val n = sc.nextInt()
    val a = Array.ofDim[Int](n,n)
    val columns = for(a_i <- 0 until n) yield {
      for(a_j <- 0 until n){
        a(a_i)(a_j) = sc.nextInt()
      }
      a(a_i).toList
    }
    n -> columns.toList
  }

  def process(size: Int, matrix: Matrix): Int = {
    val diagonales = for(i <- 0 until size) yield matrix(i)(i) -> matrix(i)(size-i-1)
    val resul = diagonales.foldLeft(0 -> 0)((a, r) => (r._1 + a._1) -> (r._2 + a._2))
    math.abs(resul._1 - resul._2)
  }
}

