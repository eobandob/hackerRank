package hackerRank.algorithms.warmup

/**
 * Created by londo on 2021-03-10.
 * https://www.hackerrank.com/challenges/birthday-cake-candles/problem
 */
object Result {

  /*
   * Complete the 'birthdayCakeCandles' function below.
   *
   * The function is expected to return an INTEGER.
   * The function accepts INTEGER_ARRAY candles as parameter.
   */

  def birthdayCakeCandles(candles: Array[Int]): Int = {
    var tallest: Int = Integer.MIN_VALUE
    var count: Int = 0
    for (candle <- candles) {
      if (candle > tallest) {
        tallest = candle
        count = 1
      } else if (candle == tallest) {
        count += 1
      }
    }
    count
  }

}

object BirthdayCakeCandles {
  def main(args: Array[String]): Unit = {
    import scala.io.StdIn

//    val printWriter = new PrintWriter(sys.env("OUTPUT_PATH"))

//    val candlesCount = StdIn.readLine.trim.toInt

//    val candles = StdIn.readLine.replaceAll("\\s+$", "").split(" ").map(_.trim.toInt)

    val candles = args.map(_.toInt)
    val result = Result.birthdayCakeCandles(candles)
    println(result)

//    printWriter.println(result)
//    printWriter.close()
  }
}

