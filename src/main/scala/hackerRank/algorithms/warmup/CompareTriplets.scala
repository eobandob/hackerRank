package hackerRank.algorithms.warmup

/**
  * Created by londo on 16/09/16.
  * https://www.hackerrank.com/challenges/compare-the-triplets
  */
object CompareTriplets {

  type Score = (Int, Int, Int)

  def main(args: Array[String]) {
    val input = scala.io.Source.fromInputStream(System.in).mkString
    val (score1, score2) = read(input)
    val res = processScores(score1, score2)
    println(s"${res._1} ${res._2}")
  }

  def read(input: String): (Score, Score) = {
    val sc = new java.util.Scanner(input)
    val a = (sc.nextInt(), sc.nextInt(), sc.nextInt())
    val b = (sc.nextInt(), sc.nextInt(), sc.nextInt())
    a -> b
  }

  def processScores(score1: Score, score2: Score): (Int, Int) = {
    List(compare(score1._1, score2._1), compare(score1._2, score2._2), compare(score1._3, score2._3))
      .foldRight(0 -> 0)((a, b) => (a._1 + b._1) -> (a._2 + b._2))
  }

  def compare(num1: Int, num2: Int): (Int, Int) = {
    if (num1 == num2) 0 -> 0
    else
      if (num1 < num2) (0, 1)
      else (1, 0)
  }

}
