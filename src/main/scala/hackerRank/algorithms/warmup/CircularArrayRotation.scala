package hackerRank.algorithms.warmup

/**
  * Created by londo on 17/09/16.
  * https://www.hackerrank.com/challenges/circular-array-rotation
  */
object CircularArrayRotation {

  def main(args: Array[String]) {
    val input = scala.io.Source.fromInputStream(System.in).mkString
    val (rotations, array, queries) = read(input)
    val newArray = rotateArray(rotations, array)
    val results = queryArray(newArray.toArray, queries)
    results.foreach(println)
  }

  def read(input: String): (Int, List[Int], List[Int]) = {
    val sc = new java.util.Scanner(input)
    val n = sc.nextInt()
    val k = sc.nextInt()
    val q = sc.nextInt()
    val arr = new Array[Int](n)
    for(arr_i <- 0 until n) {
      arr(arr_i) = sc.nextInt()
    }
    val qs = new Array[Int](q)
    for(arr_i <- 0 until q) {
      qs(arr_i) = sc.nextInt()
    }
    (k, arr.toList, qs.toList)
  }

  def rotateArray(k: Int, arr: List[Int]): List[Int] = {
    val trueRotation = k % arr.size
    arr.takeRight(trueRotation) ++ arr.dropRight(trueRotation)
  }

  def queryArray(arr: Array[Int], queries: List[Int]): List[Int] = {
    queries.map(i => arr(i))
  }

}
