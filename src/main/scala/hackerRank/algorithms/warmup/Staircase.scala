package hackerRank.algorithms.warmup

/**
  * Created by londo on 17/09/16.
  * https://www.hackerrank.com/challenges/staircase
  */
object Staircase {

  def main(args: Array[String]) {
    val sc = new java.util.Scanner(System.in)
    val n = sc.nextInt()
    buildStairs(n)
  }

  def buildStairs(n: Int): String = {
    val lines = for (i <- 1 to n) yield {
      val chars = List.fill[Char](n-i)(' ') ::: List.fill[Char](i)('#')
      chars.mkString("")
    }
    lines.mkString("\n")
  }
}
