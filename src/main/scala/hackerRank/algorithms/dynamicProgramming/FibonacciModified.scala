package hackerRank.algorithms.dynamicProgramming

import java.io._
import java.math._
import java.security._
import java.text._
import java.util._
import java.util.concurrent._
import java.util.function._
import java.util.regex._
import java.util.stream._

/**
 * Created by londo on 2021-03-10.
 * https://www.hackerrank.com/challenges/fibonacci-modified/problem
 */
object FibonacciModified {

  // Complete the fibonacciModified function below.
  def fibonacciModified(t1: Int, t2: Int, n: Int): BigInt = {
    @scala.annotation.tailrec
    def fiboModRec(a: BigInt, b: BigInt, n: BigInt): BigInt = {
      println(s"#####  a: $a, b: $b, n: $n")
      if (n == 1) a
      else {
        val c = a + b*b
        fiboModRec(b, c, n - 1)
      }
    }

    fiboModRec(t1, t2, n)

  }

  def main(args: Array[String]): Unit = {
    val stdin = scala.io.StdIn

//    val printWriter = new PrintWriter(sys.env("OUTPUT_PATH"))

    val t1T2n = args

    val t1 = t1T2n(0).trim.toInt

    val t2 = t1T2n(1).trim.toInt

    val n = t1T2n(2).trim.toInt

    val result = fibonacciModified(t1, t2, n)
    println()
    println(result)

//    printWriter.println(result)
//    printWriter.close()
  }
}

