package hackerRank.dataStructure.arrays

import org.scalatest.FunSuite

/**
  * Created by londo on 26/09/16.
  */
class LeftRotationTest extends FunSuite {

  test("read: original") {
    val input = """5 4
                  |1 2 3 4 5
                  |""".stripMargin
    val output = 4 -> List(1, 2, 3, 4, 5)
    assert(LeftRotation.read(input) == output)
  }

  test("rotation: original") {
    val rotate = 4
    val list = List(1, 2, 3, 4, 5)
    val output = List(5, 1, 2, 3, 4)
    assert(LeftRotation.rotation(rotate, list) == output)
  }

  test("rotation: manual") {
    val list = List(1, 2, 3, 4, 5)
    assert(LeftRotation.rotation(0, list) == List(1, 2, 3, 4, 5))
    assert(LeftRotation.rotation(1, list) == List(2, 3, 4, 5, 1))
    assert(LeftRotation.rotation(2, list) == List(3, 4, 5, 1, 2))
    assert(LeftRotation.rotation(3, list) == List(4, 5, 1, 2, 3))
    assert(LeftRotation.rotation(4, list) == List(5, 1, 2, 3, 4))
  }

}
