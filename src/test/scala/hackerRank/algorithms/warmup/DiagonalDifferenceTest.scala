package hackerRank.algorithms.warmup

import org.scalatest.FunSuite

/**
  * Created by londo on 25/09/16.
  */
class DiagonalDifferenceTest extends FunSuite {

  test("Read: original") {
    val input = """3
                  |11 2 4
                  |4 5 6
                  |10 8 -12""".stripMargin
    val output = 3 -> List(List(11, 2, 4), List(4, 5, 6), List(10, 8, -12))
    assert(DiagonalDifference.read(input) === output)
  }

  test("Read: made up") {
    val input = """5
                  |11 2 4 474 7
                  |4 5 6 8 100
                  |10 8 -12 58 7
                  |10 8 -12 58 7
                  |10 8 -12 58 7""".stripMargin
    val output = 5 -> List( List(11, 2,   4, 474,   7),
                            List( 4, 5,   6,   8, 100),
                            List(10, 8, -12,  58,   7),
                            List(10, 8, -12,  58,   7),
                            List(10, 8, -12,  58,   7))
    assert(DiagonalDifference.read(input) === output)
  }

  test("Process: original") {
    val (size, matrix) = 3 -> List(List(11, 2, 4), List(4, 5, 6), List(10, 8, -12))
    val output = 15
    assert(DiagonalDifference.process(size, matrix) === output)
  }

  test("Process: made up") {
    val (size, matrix) = 5 -> List(List(11, 2,   4, 474,   7),
                                   List( 4, 5,   6,   8, 100),
                                   List(10, 8, -12,  58,   7),
                                   List(10, 8, -12,  58,   7),
                                   List(10, 8, -12,  58,   7))
    val output = 48
    assert(DiagonalDifference.process(size, matrix) === output)
  }

}
