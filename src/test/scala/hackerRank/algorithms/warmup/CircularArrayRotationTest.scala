package hackerRank.algorithms.warmup

import org.scalatest.FunSuite

/**
  * Created by londo on 25/09/16.
  */
class CircularArrayRotationTest extends FunSuite {

  test("Read: original") {
    val input = """3 2 3
                  |1 2 3
                  |0
                  |1
                  |2""".stripMargin
    val output = (2, List(1, 2, 3), List(0, 1, 2))
    assert(CircularArrayRotation.read(input) == output)
  }

  test("Read: original extended") {
    val input = """4 2 3
                  |1 2 3 5
                  |0
                  |1
                  |2""".stripMargin
    val output = (2, List(1, 2, 3, 5), List(0, 1, 2))
    assert(CircularArrayRotation.read(input) == output)
  }

  test("RotateArray: original") {
    val rotations = 2
    val array = List(1, 2, 3)
    val output = List(2, 3, 1)
    assert(CircularArrayRotation.rotateArray(rotations, array) == output)
  }

  test("RotateArray: original extended") {
    val rotations = 2
    val array = List(1, 2, 3, 5)
    val output = List(3, 5, 1, 2)
    assert(CircularArrayRotation.rotateArray(rotations, array) == output)
  }

  test("QueryArray: original") {
    val array = Array(2, 3, 1)
    val queries = List(0, 1, 2)
    val output = List(2, 3, 1)
    assert(CircularArrayRotation.queryArray(array, queries) == output)
  }

  test("QueryArray: original extended") {
    val array = Array(3, 5, 1, 2)
    val queries = List(0, 1, 2)
    val output = List(3, 5, 1)
    assert(CircularArrayRotation.queryArray(array, queries) == output)
  }

}
