package hackerRank.algorithms.warmup

import org.scalatest.FunSuite

/**
  * Created by londo on 26/09/16.
  */
class TimeConversionTest extends FunSuite {

  test("time convertion: 12:00:00AM") {
    val input = "12:00:00AM"
    val output = "00:00:00"
    assert(TimeConversion.timeConvertion(input) == output)
  }

  test("time convertion: 12:00:00PM") {
    val input = "12:00:00PM"
    val output = "12:00:00"
    assert(TimeConversion.timeConvertion(input) == output)
  }

  test("time convertion: 03:00:00PM") {
    val input = "03:00:00PM"
    val output = "15:00:00"
    assert(TimeConversion.timeConvertion(input) == output)
  }

  test("time convertion: 12:30:00AM") {
    val input = "12:30:00AM"
    val output = "00:30:00"
    assert(TimeConversion.timeConvertion(input) == output)
  }

}
