package hackerRank.algorithms.warmup

import org.scalatest.FunSuite

/**
  * Created by londo on 26/09/16.
  */
class PlusMinusTest extends FunSuite {

  test("Read: original") {
    val input = """6
                  |-4 3 -9 0 4 1""".stripMargin
    val output = (6, List(-4, 3, -9, 0, 4, 1))
    assert(PlusMinus.read(input) == output)
  }

  test("Read: negatives") {
    val input = """3
                  |-4 -3 -9""".stripMargin
    val output = (3, List(-4, -3, -9))
    assert(PlusMinus.read(input) == output)
  }

  test("Read: positives") {
    val input = """3
                  |3 4 1""".stripMargin
    val output = (3, List(3, 4, 1))
    assert(PlusMinus.read(input) == output)
  }

  test("Read: zeros") {
    val input = """3
                  |+0 -0 0""".stripMargin
    val output = (3, List(0, 0, 0))
    assert(PlusMinus.read(input) == output)
  }

  test("Process: original") {
    val size = 6
    val list = List(-4, 3, -9, 0, 4, 1)
    val output = (3d/6, 2d/6, 1d/6)
    assert(PlusMinus.process(size, list) == output)
  }

  test("Process: negatives") {
    val size = 3
    val list = List(-4, -3, -9)
    val output = (0d/3, 3d/3, 0d/3)
    assert(PlusMinus.process(size, list) == output)
  }

  test("Process: positives") {
    val size = 3
    val list = List(3, 4, 1)
    val output = (3d/3, 0d/3, 0d/3)
    assert(PlusMinus.process(size, list) == output)
  }

  test("Process: zeros") {
    val size = 3
    val list = List(+0, -0, 0)
    val output = (0d/3, 0d/3, 3d/3)
    assert(PlusMinus.process(size, list) == output)
  }

}
