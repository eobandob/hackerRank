package hackerRank.algorithms.warmup

import org.scalatest.FunSuite

/**
  * Created by londo on 25/09/16.
  */
class BigSumTest extends FunSuite {

  test("Read: Sum 1 to 10") {
    val input = (1 to 10).mkString("10\n", " ", "")
    val output = (1 to 10).toList
    assert(BigSum.read(input) == output)
  }

  test("Read: Sum 1 to 4") {
    val input = "4 \n 1 2 3 4"
    val output = List(1, 2, 3, 4)
    assert(BigSum.read(input) == output)
  }

  test("Read: original") {
    val input = """5
                  |1000000001 1000000002 1000000003 1000000004 1000000005""".stripMargin
    val output = List(1000000001, 1000000002, 1000000003, 1000000004, 1000000005)
    assert(BigSum.read(input) == output)
  }

  test("Read: Int.maxValue") {
    val input = "2\n" + List(0x7fffffff, 3).mkString(" ")
    val output = List(Int.MaxValue, 3)
    assert(BigSum.read(input) == output)
  }

  test("Process: Sum 1 to 10") {
    val input = (1 to 10).toList
    val output = 55L
    assert(BigSum.process(input) == output)
  }

  test("Process: Sum 1 to 4") {
    val input = List(1, 2, 3, 4)
    val output = 10L
    assert(BigSum.process(input) == output)
  }

  test("Process: original") {
    val input = List(1000000001, 1000000002, 1000000003, 1000000004, 1000000005)
    val output = 5000000015L
    assert(BigSum.process(input) == output)
  }

  test("Process: Int.maxValue") {
    val input = List(Int.MaxValue, 3)
    val output = 2147483650L
    assert(BigSum.process(input) == output)
  }

}
