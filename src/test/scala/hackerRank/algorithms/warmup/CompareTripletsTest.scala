package hackerRank.algorithms.warmup

import org.scalatest.FunSuite
import scala.util.Random

/**
  * Created by londo on 25/09/16.
  */
class CompareTripletsTest extends FunSuite {
  def random(): Int = Random.nextInt(100) + 1

  test("Read: original") {
    val input = """5 6 7
                  |3 6 10""".stripMargin
    val output = ((5, 6, 7), (3, 6, 10))
    assert(CompareTriplets.read(input) == output)
  }

  test("Read: random") {
    val (a, b, c, d, e, f) = (random(), random(), random(), random(), random(), random())
    val input = s"""$a $b $c
                   |$d $e $f""".stripMargin
    val output = ((a, b, c), (d, e, f))
    assert(CompareTriplets.read(input) == output)
  }

  test("Process: original") {
    val input = (5, 6, 7) -> (3, 6, 10)
    val output = 1 -> 1
    assert(CompareTriplets.processScores(input._1, input._2) == output)
  }

  test("Compare: random") {
    for ( i <- 1 to 100){
      val (a, b) = random() -> random()
      val res = CompareTriplets.compare(a, b)
      assert(math.signum(res._1 - res._2) == math.signum(a - b))
    }
  }

}
