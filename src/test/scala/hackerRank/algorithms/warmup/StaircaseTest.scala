package hackerRank.algorithms.warmup

import org.scalatest.FunSuite

/**
  * Created by londo on 26/09/16.
  */
class StaircaseTest extends FunSuite {

  test("Staircase: 0") {
    val input = 0
    val output = ""
    assert(Staircase.buildStairs(input) == output)
  }

  test("Staircase: 1") {
    val input = 1
    val output = "#"
    assert(Staircase.buildStairs(input) == output)
  }

  test("Staircase: 2") {
    val input = 2
    val output = """ #
                   |##""".stripMargin
    assert(Staircase.buildStairs(input) == output)
  }

  test("Staircase: 3") {
    val input = 3
    val output = """  #
                   | ##
                   |###""".stripMargin
    assert(Staircase.buildStairs(input) == output)
  }

  test("Staircase: 4") {
    val input = 4
    val output = """   #
                   |  ##
                   | ###
                   |####""".stripMargin
    assert(Staircase.buildStairs(input) == output)
  }

  test("Staircase: 5") {
    val input = 5
    val output = """    #
                   |   ##
                   |  ###
                   | ####
                   |#####""".stripMargin
    assert(Staircase.buildStairs(input) == output)
  }

  test("Staircase: 6") {
    val input = 6
    val output = """     #
                   |    ##
                   |   ###
                   |  ####
                   | #####
                   |######""".stripMargin
    assert(Staircase.buildStairs(input) == output)
  }

}
