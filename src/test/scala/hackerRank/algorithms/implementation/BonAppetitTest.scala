package hackerRank.algorithms.implementation

import org.scalatest.FunSuite

/**
  * Created by londo on 25/09/16.
  */
class BonAppetitTest extends FunSuite {

  test("Read => Original test 1") {
    val input = """4 1
                  |3 10 2 9
                  |12""".stripMargin
    val output = (1, List(3, 10, 2, 9), 12)
    assert(BonAppetit.read(input) === output)
  }

  test("Read => Original test 2") {
    val input = """4 1
                  |3 10 2 9
                  |7""".stripMargin
    val output = (1, List(3, 10, 2, 9), 7)
    assert(BonAppetit.read(input) === output)
  }

  test("Process => Original test 1") {
    val items = List(3, 10, 2, 9)
    val charged = 12
    val notEaten = 1
    val output = "5"
    assert(BonAppetit.process(items, charged, notEaten) == output)
  }

  test("Process => Original test 2") {
    val items = List(3, 10, 2, 9)
    val charged = 7
    val notEaten = 1
    val output = "Bon Appetit"
    assert(BonAppetit.process(items, charged, notEaten) == output)
  }

}
