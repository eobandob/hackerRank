package hackerRank.algorithms.strings

import org.scalatest.FunSuite

/**
  * Created by londo on 27/09/16.
  */
class BeautifulBinaryStringTest extends FunSuite {

  test("lessMovements: original 1") {
    val input = "0101010"
    val output = 2
    assert(BeautifulBinaryString.lessMovements(input) == output)
  }

  test("lessMovements: original 2") {
    val input = "01100"
    val output = 0
    assert(BeautifulBinaryString.lessMovements(input) == output)
  }

  test("lessMovements: original 3") {
    val input = "0100101010"
    val output = 3
    assert(BeautifulBinaryString.lessMovements(input) == output)
  }

  test("lessMovements: Test Case #4") {
    val input = "0100101010100010110100100110110100011100111110101001011001110111110000101011011111011001111100011101"
    val output = 10
    assert(BeautifulBinaryString.lessMovements(input) == output)
  }

  test("lessMovements: Test Case #6") {
    val input = "100110110011111101110100011011101000011010111001001011010010110010111011100000000100011111100101010"
    val output = 11
    assert(BeautifulBinaryString.lessMovements(input) == output)
  }

  test("lessMovements: Test Case #7") {
    val input = "10110101101010001111011100100001010001111010110000111100110110111110011011000111100010011100111"
    val output = 6
    assert(BeautifulBinaryString.lessMovements(input) == output)
  }

  test("lessMovements: Test Case #8") {
    val input = "0101000010011100111110011000001000100101100010000011010111111101110110001110111110110101001011"
    val output = 9
    assert(BeautifulBinaryString.lessMovements(input) == output)
  }

}
