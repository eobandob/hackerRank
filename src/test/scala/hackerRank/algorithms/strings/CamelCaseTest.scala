package hackerRank.algorithms.strings

import org.scalatest.FunSuite

/**
  * Created by londo on 25/09/16.
  */
class CamelCaseTest extends FunSuite {

  test("Case: saveChangesInTheEditor") {
    val input = "saveChangesInTheEditor"
    val output = 5
    assert(CamelCase.wordsCount(input) == output)
  }

  test("Case: saveChangesInTheEditorA") {
    val input = "saveChangesInTheEditorA"
    val output = 6
    assert(CamelCase.wordsCount(input) == output)
  }

  test("Case: save") {
    val input = "save"
    val output = 1
    assert(CamelCase.wordsCount(input) == output)
  }

}
