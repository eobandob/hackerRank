package hackerRank.tutorial.days30

import org.scalatest.FunSuite

/**
  * Created by londo on 26/09/16.
  */
class Day6Test extends FunSuite {

  test("read: original") {
    val input = """2
                  |Hacker
                  |Rank""".stripMargin
    val output = List("Hacker", "Rank")
    assert(Day6.read(input) == output)
  }

  test("process: Hacker") {
    val input = "Hacker"
    val output = ("Hce", "akr")
    assert(Day6.process(List(input)) == List(output))
  }

  test("process: Rank") {
    val input = "Rank"
    val output = ("Rn", "ak")
    assert(Day6.process(List(input)) == List(output))
  }

  test("process: Ranks") {
    val input = "Ranks"
    val output = ("Rns", "ak")
    assert(Day6.process(List(input)) == List(output))
  }

}
