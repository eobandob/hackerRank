package hackerRank.tutorial.days30

import org.scalatest.FunSuite

/**
  * Created by londo on 26/09/16.
  */
class Day3Test extends FunSuite {

  test("Read: 3") {
    val input = "3"
    assert(Day3.read(input) == input.toInt)
  }

  test("Read: 4") {
    val input = "4"
    assert(Day3.read(input) == input.toInt)
  }

  test("Read: 32") {
    val input = "32"
    assert(Day3.read(input) == input.toInt)
  }

  test("Read: 43") {
    val input = "43"
    assert(Day3.read(input) == input.toInt)
  }

  test("isWeird: 3") {
    val input = 3
    assert(Day3.isWeird(input) == "Weird")
  }

  test("isWeird: 4") {
    val input = 4
    assert(Day3.isWeird(input) == "Not Weird")
  }

  test("isWeird: 8") {
    val input = 8
    assert(Day3.isWeird(input) == "Weird")
  }

  test("isWeird: 9") {
    val input = 9
    assert(Day3.isWeird(input) == "Weird")
  }

  test("isWeird: 21") {
    val input = 21
    assert(Day3.isWeird(input) == "Weird")
  }

  test("isWeird: 24") {
    val input = 24
    assert(Day3.isWeird(input) == "Not Weird")
  }

  test("isWeird: 32") {
    val input = 32
    assert(Day3.isWeird(input) == "Not Weird")
  }

  test("isWeird: 43") {
    val input = 43
    assert(Day3.isWeird(input) == "Weird")
  }

}
